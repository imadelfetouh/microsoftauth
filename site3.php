<?php
ini_set('log_errors_max_len', 0);
//autoload composer vendor dirs
require __DIR__ . '/vendor/autoload.php';
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
//start sessie
session_start();



 // config parameters voor magium authenticate 
$config = [
    'authentication' => [
        'ad' => [
            'client_id' => 'aa61a7e4-fafb-44bc-894b-7c1300193578',
            'client_secret' => '1a2lok11Lm25Dlr__6I_3.BgT-3a6-s9K_',
            'enabled' => 'yes',
            'directory' => 'd9d6b87a-f22a-4c99-8d5a-25ba4629b8ac',
            'return_url' => 'https://shop.kuijpers.com/microsoftauth/site3.php'
        ]
    ]
]; 



//instantiate zend http request
$request = new \Zend\Http\PhpEnvironment\Request();

//instantiate magium zelf
$ad = new \Magium\ActiveDirectory\ActiveDirectory(
    new \Magium\Configuration\Config\Repository\ArrayConfigurationRepository($config),
    Zend\Psr7Bridge\Psr7ServerRequest::fromZend(new \Zend\Http\PhpEnvironment\Request())
);

//start authenticatie
$entity = $ad->authenticate();

//luister naar get request voor ?logout
if (isset($_GET['logout']))
    {
        //verwijder sessies
        session_destroy();
        $entity = $ad->forget();
        
        //redirect naar de logout page van azure met een returnadres
        header("Location: https://login.microsoftonline.com/".$config['authentication']['ad']['directory']."/oauth2/logout?post_logout_redirect_uri=https://shop.kuijpers.com"); 
    }



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
if (isset($_SESSION['__MAGIUM_AD']['entity'])){  //check of waardes bekend zijn in session



    //get users manager from msgraph    
    $accessToken = $entity->getAccessToken();
    try
    {
        $email = $entity->getEmail();

        $_SESSION['username'] = $email;

        $graph = new Graph();
        $graph->setAccessToken($accessToken);

        ///get group from msgraph with the correct groupid. script will automatically go to exception if nothing is found
        $groups = $graph->createRequest("GET", "/users/".$entity->getOid()."/transitiveMemberOf/621dd697-2a49-46bd-b31c-3d59548ed200")
                    ->setReturnType(Model\Group::class)
                    ->execute();

        header("Location: https://shop.kuijpers.com/portaal");
    }
    catch (Exception $e)
    {
        echo "U kunt helaas niet inloggen. Neem contact op met uw manager voor meer info.";
    }
}

?>

</body>
</html>


